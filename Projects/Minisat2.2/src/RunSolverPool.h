﻿#include "../../Minisat2.2_lib/core/SolverTypes.h"
#include "../../Minisat2.2_lib/utils/Options.h"
#include <string>
#include <map>

//#define _CRT_SECURE_NO_WARNINGS 1

class RunSolverPool
{
public:

	RunSolverPool();

	Minisat::lbool Solve(int argc, char** argv) const;

	//! Options
	static Minisat::StringOption solver;
	static Minisat::IntOption    verb;
	static Minisat::BoolOption   pre;
	static Minisat::BoolOption   solve;
	static Minisat::StringOption dimacs;
	static Minisat::BoolOption   strictp;

private:

	typedef Minisat::lbool (* RunSolver)(int, char**);
	typedef std::map<std::string, RunSolver> RunSolverMap;

	RunSolverMap run_solver_map_;
};
