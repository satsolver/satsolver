﻿#include "IStream.h"
#include "RunSolverPool.h"
#include "../../Minisat2.2_lib/utils/System.h"
#include "../../Minisat2.2_lib/core/Solver.h"
#include "../../Minisat2.2_lib/core/SolverTrace.h"
#include "../../Minisat2.2_lib/simp/SimpSolver.h"
#include "../../Minisat2.2_lib/utils/SolverStateAccessor.h"

namespace
{

Minisat::Solver* solver;

// Terminate by notifying the solver and back out gracefully. This is mainly to have a test-case
// for this feature of the Solver as it may take longer than an immediate call to '_exit()'.
void SIGINT_interrupt(int)
{
	solver->interrupt();
}

// Note that '_exit()' rather than 'exit()' has to be used. The reason is that 'exit()' calls
// destructors and may cause deadlocks if a malloc/free function happens to be running (these
// functions are guarded by locks for multithreaded use).
void SIGINT_exit(int)
{
    printf("\n*** INTERRUPTED ***\n");
    if(solver->verbosity > 0)
	{
        solver->printStats();
        printf("\n*** INTERRUPTED ***\n");
	}
    _exit(1);
}

Minisat::lbool RunCoreSolver(int argc, char** argv)
{
	std::cout << "> Run core solver" << std::endl;

	using namespace Minisat;

	//! Читаем имя входного файла
	if(argc < 2)
		throw std::invalid_argument(std::string(argv[0]) + std::string(": <input-file> is not defined!"));

	const std::string in_file(argv[1]);
	

	const double initial_time = cpuTime();
	Solver S;
	S.verbosity = RunSolverPool::verb;
	solver = &S;
	// Use signal handlers that forcibly quit until the solver will be able to respond to
	// interrupts:
	sigTerm(SIGINT_exit);

	ReadProblem(in_file, S, RunSolverPool::strictp);

	if(S.verbosity > 0)
	{
		printf("============================[ Problem Statistics ]=============================\n");
		printf("|                                                                             |\n");
		printf("|  Number of variables:  %12d                                         |\n", S.nVars());
		printf("|  Parse time:           %12.2f s                                       |\n", cpuTime() - initial_time);
		printf("|                                                                             |\n");
	}

	//! Файл для сохранения результатов
	FILE* res = (argc >= 3) ? fopen(argv[2], "wb") : NULL;

	// Change to signal-handlers that will only notify the solver and allow it to terminate
	// voluntarily:
	sigTerm(SIGINT_interrupt);

	if(!S.simplify())
	{
		if(res != NULL)
		{
			fprintf(res, "UNSAT\n");
			fclose(res);
		}
		if(S.verbosity > 0)
		{
			printf("===============================================================================\n");
			printf("Solved by unit propagation\n");
			S.printStats();
			printf("\n"); 
		}
		printf("UNSATISFIABLE\n");
		exit(20);
	}

	//! Запукаем поиск решения
	vec<Lit> dummy;
	lbool ret = S.solveLimited(dummy);
	if(S.verbosity > 0)
	{
		S.printStats();
		printf("\n");
	}
	printf(ret == l_True ? "SATISFIABLE\n" : ret == l_False ? "UNSATISFIABLE\n" : "INDETERMINATE\n");

	if(res != NULL)
	{
		if(ret == l_True)
		{
			fprintf(res, "SAT\n");
			for(int i = 0; i < S.nVars(); i++)
				if(S.model[i] != l_Undef)
					fprintf(res, "%s%s%d", (i==0)?"":" ", (S.model[i]==l_True)?"":"-", i+1);
			fprintf(res, " 0\n");
		}
		else if (ret == l_False)
			fprintf(res, "UNSAT\n");
		else
			fprintf(res, "INDET\n");
		fclose(res);
	}

	return ret;
}

Minisat::lbool RunCoreSolverTrace(int argc, char** argv)
{
	std::cout << "> Run core solver trace" << std::endl;

	using namespace Minisat;

	//! Читаем имя входного файла
	if(argc < 2)
		throw std::invalid_argument(std::string(argv[0]) + std::string(": <input-file> is not defined!"));

	const std::string in_file(argv[1]);
	const double initial_time = cpuTime();
	SolverTrace S;
	S.verbosity = RunSolverPool::verb;
	//solver = &S;
	// Use signal handlers that forcibly quit until the solver will be able to respond to
	// interrupts:
	//sigTerm(SIGINT_exit);

	std::cout << "Begin read problem" << std::endl;
	ReadProblem(in_file, S, RunSolverPool::strictp);
	std::cout << "End read problem" << std::endl;

	if(S.verbosity > 0)
	{
		printf("============================[ Problem Statistics ]=============================\n");
		printf("|                                                                             |\n");
		printf("|  Number of variables:  %12d                                         |\n", S.nVars());
		printf("|  Parse time:           %12.2f s                                       |\n", cpuTime() - initial_time);
		printf("|                                                                             |\n");
	}

	//! Файл для сохранения результатов
	FILE* res = (argc >= 3) ? fopen(argv[2], "wb") : NULL;

	// Change to signal-handlers that will only notify the solver and allow it to terminate
	// voluntarily:
	//sigTerm(SIGINT_interrupt);

	if(!S.simplify())
	{
		if(res != NULL)
		{
			fprintf(res, "UNSAT\n");
			fclose(res);
		}
		if(S.verbosity > 0)
		{
			printf("===============================================================================\n");
			printf("Solved by unit propagation\n");
			S.printStats();
			printf("\n"); 
		}
		printf("UNSATISFIABLE\n");
		exit(20);
	}

	//! Запукаем поиск решения
	vec<Lit> dummy;
	lbool ret = S.solveLimited(dummy);
	if(S.verbosity > 0)
	{
		S.printStats();
		printf("\n");
	}
	printf(ret == l_True ? "SATISFIABLE\n" : ret == l_False ? "UNSATISFIABLE\n" : "INDETERMINATE\n");

	if(res != NULL)
	{
		if(ret == l_True)
		{
			fprintf(res, "SAT\n");
			for(int i = 0; i < S.nVars(); i++)
				if(S.model[i] != l_Undef)
					fprintf(res, "%s%s%d", (i==0)?"":" ", (S.model[i]==l_True)?"":"-", i+1);
			fprintf(res, " 0\n");
		}
		else if (ret == l_False)
			fprintf(res, "UNSAT\n");
		else
			fprintf(res, "INDET\n");
		fclose(res);
	}

	return ret;
}

Minisat::lbool RunFromState(int argc, char** argv)
{
	std::cout << "> Run from state" << std::endl;

	using namespace Minisat;
	//! Читаем имя входного файла
	if(argc < 2)
		throw std::invalid_argument(std::string(argv[0]) + std::string(": <input-file> is not defined!"));

	//! Блоб, содержащий состояние солвера
	const std::string in_file(argv[1]);
	const double initial_time = cpuTime();
	Solver S;
	S.verbosity = RunSolverPool::verb;
	solver = &S;
	// Use signal handlers that forcibly quit until the solver will be able to respond to
	// interrupts:
	sigTerm(SIGINT_exit);

	{
		SolverStateAccessor reader(S);
		reader.ReadStateBlob(in_file);
	}

	if(S.verbosity > 0)
	{
		printf("============================[ Problem Statistics ]=============================\n");
		printf("|                                                                             |\n");
		printf("|  Number of variables:  %12d                                         |\n", S.nVars());
		printf("|  Parse time:           %12.2f s                                       |\n", cpuTime() - initial_time);
		printf("|                                                                             |\n");
	}

	//! Файл для сохранения результатов
	FILE* res = (argc >= 3) ? fopen(argv[2], "wb") : NULL;

	// Change to signal-handlers that will only notify the solver and allow it to terminate
	// voluntarily:
	sigTerm(SIGINT_interrupt);

	if(!S.simplify())
	{
		if(res != NULL)
		{
			fprintf(res, "UNSAT\n");
			fclose(res);
		}
		if(S.verbosity > 0)
		{
			printf("===============================================================================\n");
			printf("Solved by unit propagation\n");
			S.printStats();
			printf("\n"); 
		}
		printf("UNSATISFIABLE\n");
		exit(20);
	}

	//! Запукаем поиск решения
	vec<Lit> dummy;
	lbool ret = S.solveLimited(dummy);
	if(S.verbosity > 0)
	{
		S.printStats();
		printf("\n");
	}
	printf(ret == l_True ? "SATISFIABLE\n" : ret == l_False ? "UNSATISFIABLE\n" : "INDETERMINATE\n");

	if(res != NULL)
	{
		if(ret == l_True)
		{
			fprintf(res, "SAT\n");
			for(int i = 0; i < S.nVars(); i++)
				if(S.model[i] != l_Undef)
					fprintf(res, "%s%s%d", (i==0)?"":" ", (S.model[i]==l_True)?"":"-", i+1);
			fprintf(res, " 0\n");
		}
		else if (ret == l_False)
			fprintf(res, "UNSAT\n");
		else
			fprintf(res, "INDET\n");
		fclose(res);
	}

	return ret;
}

Minisat::lbool RunSimpSolver(int argc, char** argv)
{
	std::cout << "> Run simp solver" << std::endl;

	using namespace Minisat;
	//! Читаем имя входного файла
	if(argc < 2)
		throw std::invalid_argument(std::string(argv[0]) + std::string(": <input-file> is not defined!"));

	const std::string in_file(argv[1]);
	double initial_time = cpuTime();
	double parse_time(0);
	double simplified_time(0);
	SimpSolver S;
	S.verbosity = RunSolverPool::verb;
	solver = &S;
	// Use signal handlers that forcibly quit until the solver will be able to respond to
	// interrupts:
	sigTerm(SIGINT_exit);

	//! Читаем формулу из файла
	ReadProblem(in_file, S, RunSolverPool::strictp);
        
	if(S.verbosity > 0)
	{
		parse_time = cpuTime();
		printf("============================[ Problem Statistics ]=============================\n");
		printf("|                                                                             |\n");
		printf("|  Number of variables:  %12d                                         |\n", S.nVars());
		printf("|  Number of clauses:    %12d                                         |\n", S.nClauses());
		printf("|  Parse time:           %12.2f s                                       |\n", parse_time - initial_time);
	}
    
	FILE* res = (argc >= 3) ? fopen(argv[2], "wb") : NULL;

	// Change to signal-handlers that will only notify the solver and allow it to terminate
	// voluntarily:
	sigTerm(SIGINT_interrupt);

	S.eliminate(true);
	if(S.verbosity > 0)
	{
		simplified_time = cpuTime();
		printf("|  Simplification time:  %12.2f s                                       |\n", simplified_time - parse_time);
		printf("|                                                                             |\n");
	}

	if(!S.okay())
	{
		if (res != NULL) fprintf(res, "UNSAT\n"), fclose(res);
		if (S.verbosity > 0)
		{
			printf("===============================================================================\n");
			printf("Solved by simplification\n");
			S.printStats();
			printf("\n");
		}
		printf("UNSATISFIABLE\n");
		exit(20);
	}

	lbool ret = l_Undef;
	if(RunSolverPool::solve)
	{
		vec<Lit> dummy;
		ret = S.solveLimited(dummy);
	}
	else if (S.verbosity > 0)
		printf("===============================================================================\n");

	if(RunSolverPool::dimacs && ret == l_Undef)
		S.toDimacs((const char*)RunSolverPool::dimacs);

	if(S.verbosity > 0)
	{
		S.printStats();
		printf("\n");
	}
	printf(ret == l_True ? "SATISFIABLE\n" : ret == l_False ? "UNSATISFIABLE\n" : "INDETERMINATE\n");

	if (res != NULL)
	{
		if (ret == l_True)
		{
			fprintf(res, "SAT\n");
			for (int i = 0; i < S.nVars(); i++)
				if (S.model[i] != l_Undef)
					fprintf(res, "%s%s%d", (i==0)?"":" ", (S.model[i]==l_True)?"":"-", i+1);
			fprintf(res, " 0\n");
		}
		else if (ret == l_False)
			fprintf(res, "UNSAT\n");
		else
			fprintf(res, "INDET\n");
		fclose(res);
	}
	return ret;
}

} // namespace

Minisat::StringOption RunSolverPool::solver ("MAIN", "solver", "Type of solver (core, simp, core-trace)", "core-trace");
Minisat::IntOption    RunSolverPool::verb   ("MAIN", "verb",   "Verbosity level (0=silent, 1=some, 2=more).", 1, Minisat::IntRange(0, 2));
Minisat::BoolOption   RunSolverPool::pre    ("MAIN", "pre",    "Completely turn on/off any preprocessing.", true);
Minisat::BoolOption   RunSolverPool::solve  ("MAIN", "solve",  "Completely turn on/off solving after preprocessing.", true);
Minisat::StringOption RunSolverPool::dimacs ("MAIN", "dimacs", "If given, stop after preprocessing and write the result to this file.");
Minisat::BoolOption   RunSolverPool::strictp("MAIN", "strict", "Validate DIMACS header during parsing.", false);

RunSolverPool::RunSolverPool()
{
	run_solver_map_.insert(std::make_pair("core", RunCoreSolver));
	run_solver_map_.insert(std::make_pair("simp", RunSimpSolver));
	run_solver_map_.insert(std::make_pair("state", RunFromState));
	run_solver_map_.insert(std::make_pair("core-trace", RunCoreSolverTrace));
}

Minisat::lbool RunSolverPool::Solve(int argc, char** argv) const
{
	const std::string sname(solver);
	RunSolverMap::const_iterator it = run_solver_map_.find(sname);
	if(it == run_solver_map_.end())
	{
		throw std::invalid_argument("RunSolver::Solve: undefined type of solver.");
	}
	return it->second(argc, argv);
}
