﻿#include <iostream>
#include <fstream>
#include <string>
#include "../../Minisat2.2_lib/core/Dimacs.h"

class IStream
{
	enum { buffer_size = 64*1024 };
public:
	IStream(std::istream& s = std::cin);
	~IStream();

	char operator * () {return (pos_ < size_) ? buffer_[pos_] : EOF;}
	
	void operator ++ () {pos_++; assureLookahead();}
	
	char operator[](int index) const { return (pos_ + index < size_ ? buffer_[pos_ + index] : EOF);}

	unsigned  position() const {return pos_;}

	//! Проверяем, что и в буфере и в потоке больше нет данных
	bool eof() const {return (pos_ >= size_) && in.eof();}

private:

	void assureLookahead();

	std::istream& in;
	char* buffer_;
	//! позиция текущего символа в буфере
	unsigned pos_;
	//! число символов в буфере (size_ <= buffer_size)
	unsigned size_;
};

static inline bool isEof(IStream& in) { return *in == EOF;}

template<typename Solver>
void ReadProblem(const std::string& filename, Solver& s, bool strictp = false)
{
	std::ifstream file(filename.c_str(), std::ios::in);
	if(!file.is_open())
	{
		throw std::runtime_error(std::string("Can't open file ") + filename);
	}
	IStream in(file);
	Minisat::parse_DIMACS_main(in, s, strictp);
}
