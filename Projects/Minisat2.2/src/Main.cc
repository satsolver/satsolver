﻿#include <iostream>
#include "RunSolverPool.h"
#include "../../Minisat2.2_lib/utils/System.h"

int main(int argc, char** argv)
{
    try
	{
		using namespace Minisat;
		setUsageHelp(
			"USAGE: %s [options] <input-file> <result-output-file>\n\n"
			"  where input may be either in plain or gzipped DIMACS.\n"
			);
        setX86FPUPrecision();
		
		//! Limit options (only for linux)
		IntOption cpu_lim("MAIN", "cpu-lim","Limit on CPU time allowed in seconds.\n", 0, IntRange(0, INT32_MAX));
		IntOption mem_lim("MAIN", "mem-lim","Limit on memory usage in megabytes.\n", 0, IntRange(0, INT32_MAX));

		RunSolverPool spool;
		//! Разбор параметров и установки им переданных значений вместо значений "по умолчанию"
		parseOptions(argc, argv, true);

		// Try to set resource limits:
        if (cpu_lim != 0) 
			limitTime(cpu_lim);
        if (mem_lim != 0) 
			limitMemory(mem_lim);
		
		lbool result = spool.Solve(argc, argv);
		return (result == l_True ? 10 : result == l_False ? 20 : 0);

    }
	catch (const std::invalid_argument& e)
	{
		std::cerr << "[ERROR]: (invalid_argument): " << e.what() << std::endl;
		return -1;
	}
	catch (const std::runtime_error& e)
	{
		std::cerr << "[ERROR]: (runtime_error): " << e.what() << std::endl;
		return -1;
	}
	catch (const std::exception& e)
	{
		std::cerr << "[ERROR]: " << e.what() << std::endl;
		return -1;
	}
	catch (const Minisat::OutOfMemoryException&)
	{
		std::cerr << "===============================================================================" << std::endl;
		std::cerr << "INDETERMINATE: out of memory exception.\n" << std::endl;
        return -2;
    }
	return 0;
}
