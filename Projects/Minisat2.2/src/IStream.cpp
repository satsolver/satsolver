﻿#include "IStream.h"

IStream::IStream(std::istream& s)
	: in(s)
	, pos_(0)
	, size_(0)
{
	buffer_ = new char[buffer_size];
	assureLookahead();
}

IStream::~IStream()
{
	delete[] buffer_;
}

void IStream::assureLookahead()
{
	if(pos_ >= size_)
	{
		in.read(buffer_, buffer_size);
		size_ = static_cast<unsigned>(in.gcount());
		pos_ = 0;
	}
}

