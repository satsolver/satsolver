#include "Solver.h"

/**
* to do:
* 1. ��������� ������������ �������� �������� ������������.
* 2. ���������� ��� ����� �������� � ������������ ������� ����������.
* 3. �������� �� ������ ��� ���������.
* 4. ����� �������� ��������� ������������� - ����������� ��������������� ���������� � ��������� �������.
     ��������� �� unit-propagation ���������� �� ������ ����������� � ��������.
	 ����� �������� ������� ����������, �.�. ��� �������� ���������, ����� ������ ��� ������, �� ����� ����� �� ����.
* 5. �������� ������ watched-���������, ����� �������� ������� ������������ ����� ����������� � ���.
*/

int main(int argc, char** argv)
{
	if (argc < 2)
	{
		std::cerr << "[main]: Invalid argument: expect file name for CNF." << std::endl;
		return -1;
	}

	const char* filename = argv[1];

	MergeSolver::Solver solver;

	if (!solver.ParseDimacs(filename))
	{
		std::cerr << "[main]: Parsing of CNF is failed." << std::endl;
		return -1;
	}
	
	const std::string parsed_cnf("parsed_cnf.cnf");
	solver.WriteDimacs(parsed_cnf);

	solver.InitializeSolver();
	solver.TestPermutation();

	
	solver.Solve();

	return 0;
}
