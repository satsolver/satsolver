#pragma once
#include "Types.h"
#include "TraceLog.h"

namespace MergeSolver
{

/**
* \brief ��������� ����� ������������ ������ ��� ������ ��������� �������� ����������.
*/
class Solver
{
public:

	Solver();

	bool ParseDimacs(const char* file_name);

	void WriteDimacs(const std::string& filename) const;

	/**
	* \brief ���������� ���������� �������� � ������� ������.
	*        ��������������, ��� ��� ��������� � ��������.
	*/
	void InitializeSolver();

	/**
	* \brief ��������� ����������, ������� ���������� ��� �������� ������������.
	*/
	//void UsedVariableSet();

	/**
	* \brief ������ ��������� �������.
	*/
	void Solve();

	void TestPermutation();

private:

	/**
	* \brief ����������� ������ ���������� ���������� � ��� � ������� ����.
	* \return ����� ����������� ����������.
	*/
	size_t SubstituteAssigns();

	/**
	* \brief ��������� ������� ���������� � ������������ � �������� ����������� (����� ���������� ������������)
	*/
	void MakeMergeVariables();

	/**
	* \brief ����������� ���������� ��������� � ��������� �������.
	*        ������������ ��� �������� ����������.
	*/
	void PermutateVars();

	/**
	* \brief ���������� ����������� ��� �������� ���������, ���� ������� �������� ������.
	*/
	bool UpdatePointRecord(MergePoint& merge_var);

	void SetMergePointValue(MergePoint& point, unsigned value);

	// ��������������� ������� ��� �������� ���
	void skipWhitespace(IStream& in);
	void skipLine(IStream& in);
	bool match(IStream& in, const char *str);
	int parseInt(IStream& in);
	void readClause(IStream& in);

private:

	// �������� ����������
	MergePoints merge_points;

	// ����������� ����� ��������
	size_t merge_size;
	size_t merge_point_count = 0;
	
	// ������� ����������� ������ ����������
	size_t merge_counter = 0;
	// ������� ����������� ����������� ����������
	size_t substitution_counter = 0;

	// ����� �����������, � ������� ���������� ��������
	int current_merge_point = 0;
	// �����, � ������� ��������� ��� ��� �������� ������
	int last_record_merge_point = 0;

	// ������������ ����������
	std::vector<Var> vars_permutation;

	// ����� ���������� ����������
	std::vector<char> assigns;
	size_t vars_count = 0;

	// ����� ������ ����������� ��������� ����� ����������� ����������
	std::vector<size_t> clauses_satisfied;
	size_t max_sat_clauses = 0;
	size_t clauses_count = 0;

	double parse_time = 0.;

	// �������� ���, ����������� �� �����
	Problem cnf_;

	bool use_trace = true;
	TraceLog log_;
};

} // namespace MergeSolver