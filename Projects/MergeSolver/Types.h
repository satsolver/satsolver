#pragma once
#include <cstdint>
#include <vector>
#include <istream>
#include <iostream>
#include <memory>

namespace MergeSolver
{

// ������������� ������� ���������� � ��������
typedef int Var;

/**
* \brief ������� ��� ������� ����������.
*/
class Lit
{
	int x;
public:
	
	/**
	* \brief ������������ ������� ��� ����������.
	*        ��� �������� ���� �������� ������������ ������� ������.
	*        ������� ��������� ����������, ���� � ������� ������� 0.
	*
	* \param var   - ����� ����������
	* \param phase - ���� ��������
	*/
	Lit(Var var = 0, bool phase = false)
		: x((var + var) + (int)phase)
	{}

	// Don't use these for constructing/deconstructing literals. Use the normal constructors instead.
	friend int  toInt(Lit p);  // Guarantees small, positive integers suitable for array indexing.
	friend Lit  toLit(int i);  // Inverse of 'toInt()'
	friend Lit  operator   ~(Lit p);  // ����� ���� (�����) �������� �� ����������������� (�������� ����� ���������)
	friend bool sign(Lit p);  // ������� ���� ��������
	friend int  var(Lit p);  // ����� ���������� (��� ����)
	friend Lit  unsign(Lit p);  // ������� ��� ����� (� ������������� ����)
	friend Lit  id(Lit p, bool sgn);

	bool operator == (Lit p) const { return x == p.x; }
	bool operator != (Lit p) const { return x != p.x; }
	bool operator <  (Lit p) const { return x < p.x; }
};

inline  int  toInt(Lit p)           { return p.x; }
inline  Lit  toLit(int i)           { Lit p; p.x = i; return p; }
inline  Lit  operator   ~(Lit p)           { Lit q; q.x = p.x ^ 1; return q; }
inline  bool sign(Lit p)           { return p.x & 1; }
inline  int  var(Lit p)           { return p.x >> 1; }
inline  Lit  unsign(Lit p)           { Lit q; q.x = p.x & ~1; return q; }
inline  Lit  id(Lit p, bool sgn) { Lit q; q.x = p.x ^ (int)sgn; return q; }

// �������������� �������
const Lit lit_Undef(-1, false);

const char l_Undef = -1;
const char l_False = 0;
const char l_True = 1;

/**
* \brief ��������.
*/
typedef std::vector<Lit> Clause;
typedef std::vector<Clause> ClauseVec;

/**
* \brief ������������� ���.
*/
class Problem
{
public:

	// ����� ����������, ����������� � ��������� dimacs-�������
	size_t header_var_count = 0;

	// ������������ ����� ����������
	// ���������� ���������� � ���� (����������� ������ minisat)
	int max_var_num = 0;

	// ����� ����������
	ClauseVec clauses;
};

/**
* \brief ������� ����� ��� ������ ���.
*/
class IStream
{
	std::istream& in;
	char c;
public:

	IStream(std::istream& s = std::cin)
		: in(s), c(0)
	{
		in.get(c);
	}

	char operator * () { return c; }

	void operator ++ () { in.get(c); }

	bool eof() const { return in.eof(); }
};

struct MergePoint
{
	MergePoint(size_t size = 3, size_t offset = 0)
		: merge_size(size)
		, merge_offset(offset)
	{}

	// ����� ����������, �������� � ����
	size_t merge_size;

	// ����� �����
	size_t merge_offset = 0;

	// �������� � �����
	unsigned value = 0;

	unsigned GetValueBound() const { return static_cast<unsigned>(1) << merge_size; }

};

typedef std::vector<MergePoint> MergePoints;

} // namespace MergeSolver
