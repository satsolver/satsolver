#include "Solver.h"
#include <fstream>
#include <ctime>

namespace MergeSolver 
{

inline double cpuTime(void) { return (double)clock() / CLOCKS_PER_SEC; }

Solver::Solver()
{}

void Solver::InitializeSolver()
{
	clauses_satisfied.reserve(1000000);
	
	// � �������� ��������� ����� ����� ��� ����
	vars_count = cnf_.max_var_num + 1;
	assigns.resize(vars_count, l_False);

	// ��������� ������������
	vars_permutation.resize(vars_count, 0);
	for (size_t i = 0; i < vars_count; ++i)
	{
		vars_permutation[i] = static_cast<Var>(i);
	}
	//std::cout << "Init permutation:";
	//for (const auto v : vars_permutation)
	//{
	//	std::cout << " " << v;
	//}
	//std::cout << std::endl;

	merge_size = 3;

	const size_t mod = vars_count % merge_size;
	merge_point_count = (vars_count / merge_size) + (mod != 0 ? 1 : 0);
	//std::cout << "Merge points: " << merge_point_count << std::endl;
	merge_points.resize(merge_point_count);
	for (size_t i = 0; i < merge_points.size(); ++i)
	{
		MergePoint& point = merge_points[i];
		point.merge_offset = i * merge_size;
		point.merge_size = 3;
		point.value = 0;
	}


	// � ��������� ����� ����� ���� ������ 3� ����������
	if (mod != 0)
	{
		merge_points.back().merge_size = mod;
	}
	
	//std::cout << "Merge point size: " << merge_size << std::endl;
	//std::cout << "Last merge point size: " << merge_points.back().merge_size << std::endl;

	merge_counter = 0;

	// ������ �� ��������� ����
	max_sat_clauses = SubstituteAssigns();
	clauses_count = cnf_.clauses.size();
	clauses_satisfied.push_back(max_sat_clauses);

	//std::cout << "Clause count: " << clauses_count << std::endl;
	//std::cout << "Max sat clauses: " << max_sat_clauses << std::endl;
	log_.var_count = vars_count;
	log_.clause_count = clauses_count;
	log_.clauses_satisfy_records.push_back({ max_sat_clauses, merge_counter });
	log_.merge_steps.push_back(TraceLog::MergeStep());
	log_.merge_steps[merge_counter].merge_points = merge_points;
	log_.merge_steps[merge_counter].vars_permutation = vars_permutation;
	TraceLog::MergePointValue mpv;
	mpv.clause_satisfied = max_sat_clauses;
	mpv.current_record = max_sat_clauses;
	mpv.var_assigns = assigns;
	mpv.time_mark = cpuTime();
	log_.merge_steps[merge_counter].merge_points_values.push_back(mpv);
}

void Solver::Solve()
{
	printf("============================[ Problem Statistics ]=============================\n");
	printf("|                                                                             |\n");
	printf("|  Number of variables:  %12d                                         |\n", vars_count);
	printf("|  Parse time:           %12.2f s                                       |\n", parse_time);
	printf("|                                                                             |\n");
	printf("===============================================================================\n");
	printf("Merge \t Sat clause \t Sat % \t Time \t Substitute\n");

	const double start_solve_time = cpuTime();

	while (max_sat_clauses < clauses_count)
	{
		current_merge_point = 0;
		last_record_merge_point = 0;

		// ������� ����� ��������� �������
		MakeMergeVariables();

		// ����� ���������� ���������� ����� �����������, ���� ���� ���� ��� �� ���� ������� �������� ������� ������
		do
		{
			if (UpdatePointRecord(merge_points[current_merge_point]))
			{
				last_record_merge_point = current_merge_point;
			}
			current_merge_point = (current_merge_point + 1) % merge_point_count;
		} while (current_merge_point != last_record_merge_point);

		// ����� ���������� �� ����������� ���������� ��������
		double sat_percent = static_cast<double>(max_sat_clauses) / clauses_count;
		double current_time = cpuTime() - start_solve_time;
		printf("%d \t %d \t %2.2f \t %3.2f \t %d\n", merge_counter, max_sat_clauses, sat_percent, current_time, substitution_counter);

		if (merge_counter >= 50)
		{
			break;
		}
	}
	printf("===============================================================================\n");
}

void Solver::SetMergePointValue(MergePoint& merge_point, unsigned merge_value)
{
	merge_point.value = merge_value;
	for (size_t i = 0; i < merge_point.merge_size; ++i)
	{
		Var var_index = vars_permutation[merge_point.merge_offset + i];
		char var_value = ((merge_value >> i) & 1) ? l_True : l_False;
		assigns[var_index] = var_value;
	}
}

bool Solver::UpdatePointRecord(MergePoint& merge_var)
{
	const unsigned var_value_bound = merge_var.GetValueBound();
	const unsigned var_value_start = merge_var.value;
	unsigned var_value_record = var_value_start;
	unsigned var_value_next = (var_value_start + 1) % var_value_bound;
	while (var_value_next != var_value_start)
	{
		// ���������� ����� ����� �����������
		SetMergePointValue(merge_var, var_value_next);

		// ��������� �������� ���� � ���� �����
		const size_t sat_clauses_count = SubstituteAssigns();
		if (max_sat_clauses < sat_clauses_count)
		{
			// �������� ������ � ��������� ��� �����
			max_sat_clauses = sat_clauses_count;
			var_value_record = var_value_next;
		}

		// ��������� ����� �����������
		var_value_next = (var_value_next + 1) % var_value_bound;
	}

	// �������� �� ���������� ������ �������
	if (var_value_record != var_value_start)
	{
		SetMergePointValue(merge_var, var_value_record);
		clauses_satisfied.push_back(max_sat_clauses);
		return true;
	}

	// ����� ������ �� ������ - ������������ � ��������� ����� �����������
	SetMergePointValue(merge_var, var_value_start);
	return false;
}

void Solver::MakeMergeVariables()
{
	if (merge_counter != 0)
	{
		// ��������� ������������ ���������� ��������� ������� 
		PermutateVars();

		// ��������� ���������� value � ������������ � ������� ������������� � ���������� ���������� � assigns
		for (MergePoint& point : merge_points)
		{
			point.value = 0;
			for (size_t i = 0; i < point.merge_size; ++i)
			{
				if (assigns[point.merge_offset + i] == l_True)
				{
					point.value |= static_cast<unsigned>(1) << i;
				}
			}
		}
	}

	++merge_counter;
}

void Solver::PermutateVars()
{
	if (vars_permutation.size() < 2)
	{
		return;
	}
	srand(static_cast<unsigned>(time(0)));
	for (size_t i = 0; i < vars_permutation.size() - 1; ++i)
	{
		// j - ��������� ����� �� [i + 1, vars_permutaion.size())
		size_t range = vars_permutation.size() - (i + 1);
		size_t j = static_cast<size_t>(rand()) % range + (i + 1);
		std::swap(vars_permutation[i], vars_permutation[j]);
	}

	log_.merge_steps.back().vars_permutation = vars_permutation;
}

void Solver::TestPermutation()
{
	std::ofstream file("vars_permutation.txt", std::ios::out);
	for (const auto& var : vars_permutation)
	{
		file << var << " ";
	}
	file << std::endl;
	for (size_t i = 0; i < 10; ++i)
	{
		PermutateVars();
		for (const auto& var : vars_permutation)
		{
			file << static_cast<unsigned>(var) << " ";
		}
		file << std::endl;
	}
}

size_t Solver::SubstituteAssigns()
{
	size_t cnt = 0;
	for (const Clause& clause : cnf_.clauses)
	{
		for (const Lit& lit : clause)
		{
			const bool is_positive = sign(lit);
			const char var_value = assigns[var(lit)];
			if (var_value == l_True && is_positive ||
				var_value == l_False && !is_positive)
			{
				++cnt;
				break;
			}
		}
	}
	++substitution_counter;
	return cnt;
}

void Solver::WriteDimacs(const std::string& filename) const
{
	std::ofstream file(filename, std::ios::out);
	file << "p cnf " << cnf_.header_var_count << " " << cnf_.clauses.size() << std::endl;
	for (const Clause& clause : cnf_.clauses)
	{
		for (const Lit& lit : clause)
		{
			file << (sign(lit) ? "-" : "") << (var(lit) + 1) << " ";
		}
		file << "0\n";
	}
}

bool Solver::ParseDimacs(const char* file_name)
{
	double start_time = cpuTime();
	std::ifstream file(file_name, std::ios::in);
	if (!file.is_open())
	{
		return false;
	}

	IStream in(file);
	for (;;)
	{
		skipWhitespace(in);
		if (in.eof()) break;
		// ��������� DIMACS-�����
		else if ((*in == 'p') && match(in, "p cnf"))
		{
			int vars = parseInt(in);
			int clauses = parseInt(in);
			if (static_cast<size_t>(vars) > cnf_.header_var_count)
			{
				cnf_.header_var_count = vars;
			}
			skipLine(in);
		}
		// ���������� �����������
		else if (*in == 'c')
		{
			skipLine(in);
		}
		else
		{
			readClause(in);
		}
	}
	parse_time = cpuTime() - start_time;
	return true;
}

void Solver::skipWhitespace(IStream& in)
{
	while ((*in >= 9 && *in <= 13) || *in == 32)
	{
		++in;
		if (in.eof()) return;
	}
}

void Solver::skipLine(IStream& in)
{
	while (in.eof() || *in != '\n') ++in;
	if (*in == '\n') ++in;
}

bool Solver::match(IStream& in, const char *str)
{
	for (; *str != 0; ++str, ++in)
		if (*str != *in)
			return false;
	return true;
}

int Solver::parseInt(IStream& in)
{
	int     val = 0;
	bool    neg = false;
	skipWhitespace(in);
	if (*in == '-') neg = true, ++in;
	else if (*in == '+') ++in;
	if (*in < '0' || *in > '9') std::cout << "PARSE ERROR! Unexpected char:" << *in << std::endl, exit(1);
	while (*in >= '0' && *in <= '9')
		val = val * 10 + (*in - '0'),
		++in;
	return neg ? -val : val;
}

void Solver::readClause(IStream& in)
{
	int var, lit;
	Clause clause;
	for (;;)
	{
		lit = parseInt(in);
		if (lit == 0) break;
		// ���������� ���������� � ����
		var = abs(lit) - 1;
		if (var > cnf_.max_var_num)
		{
			cnf_.max_var_num = var;
		}
		clause.push_back((lit > 0) ? Lit(var) : ~Lit(var));
	}
	cnf_.clauses.push_back(clause);
}

} // namespace MergeSolver
