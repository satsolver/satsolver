#pragma once
#include "Types.h"

namespace MergeSolver
{

struct TraceLog
{
	struct MergePointValue
	{
		size_t index = 0;
		size_t merge_point_index = 0;
		std::vector<char> var_assigns;
		size_t clause_satisfied = 0;
		size_t current_record = 0;
		double time_mark = 0.0;
	};

	struct MergeStep
	{
		// ������������ ����������
		std::vector<Var> vars_permutation;
		// ����-�����, ������������ ����������� ��������
		MergePoints merge_points;
		// ������� �����������
		std::vector<MergePointValue> merge_points_values;
	};

	// ����� ����� �������� ����������
	size_t var_count = 0;
	size_t clause_count = 0;
	std::vector<std::pair<size_t, size_t>> clauses_satisfy_records;
	std::vector<MergeStep> merge_steps;
};

typedef std::shared_ptr<TraceLog> TraceLogPtr;

} // namespace MergeSolver